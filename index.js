const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const PORT = 3000;

app.use(bodyParser.json());

app.get("", function (req, res) {
  res.send("Get request");
});

const processSomething = (callback) => {
  setTimeout(callback, 20000);
};

app.post("/hook", (req, res, next) => {
  processSomething(() => {
    const webhookUrl = req.params.url;
  });

  res.status(200).send("OK");
});

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
